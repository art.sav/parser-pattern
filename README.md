Usage


$parser = \VerificationParser\ParserFactory::make('\VerificationParser\SmsParser', '\VerificationParser\Patterns\YaMoneyPattern');

try {
    print_r($parser->parseText($text));
} catch (\Exception $e) {
    print_r($e->getMessage());
}
