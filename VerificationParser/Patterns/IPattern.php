<?php

namespace VerificationParser\Patterns;

/**
 * interface pattern for parse
 */
interface IPattern
{
    /**
     * Return array patterns (for preg_match) and exception for validate text
     * example:
     *    [
     *      [
     *        pattern => (string) '/text/'
     *        exception => (string) '\VerificationParser\Exceptions\NoMoneyException'
     *      ]
     *    ]
     * @return array;
     */
    public function getValidationPatterns(): array;


    /**
     * Return array pattern for parsing data
     * every regex (for preg_match) must be contain submask with name
     * every key in array must match with return name submask
     * example:
     *    [
     *      'EXAMPLE' => (string) '/(?<EXAMPLE>[0-9]{4,5})/',
     *    ]
     * @return array;
     */
    public function getDataPatterns(): array;

}
