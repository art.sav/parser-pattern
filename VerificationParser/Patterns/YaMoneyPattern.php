<?php

namespace VerificationParser\Patterns;

/**
 * Patterns for Yandex Money sms confirmations
 */
class YaMoneyPattern implements IPattern
{

    public function getValidationPatterns(): array
    {
        return [
            [ 'pattern' => '/Недостаточно/iu', 'exception' => '\VerificationParser\Exceptions\NoMoneyException'],
        ];
    }

    public function getDataPatterns(): array
    {
        return [
            'password' => '/\:(\s|)(?<password>[0-9]{4,5})/',
            'to_wallet' => '/(?<to_wallet>[0-9]{14,15})/',
            'amount' => '/(?<amount>[0-9\,]{1,10}р)/iu',
        ];
    }

}
