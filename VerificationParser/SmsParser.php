<?php

namespace VerificationParser;


/**
 * Parser sms by IPattern
 */
class SmsParser extends BaseParser\BaseParser
{

    /**
     * Parser text
     *
     * @param String text for parsing
     * @return array;
     */
    public function parseText(String $text): array
    {
        $text = strip_tags($text);

        $validationPatterns = $this->pattern->getValidationPatterns();

        foreach ($validationPatterns as $key => $patternValidate) {
            if (preg_match($patternValidate['pattern'], $text)) {
                throw new $patternValidate['exception']($text, 1);
            }
        }

        $dataPatterns = $this->pattern->getDataPatterns();

        $data = [];

        foreach ($dataPatterns as $key => $regexPattern) {
            $val = [];
            preg_match($regexPattern, $text, $val);

            if (!empty($val[$key])) {
                $data[$key] = $val[$key];
            }
        }

        return $data;
    }

}
