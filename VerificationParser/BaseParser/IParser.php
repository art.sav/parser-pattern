<?php

namespace VerificationParser\BaseParser;

/**
 * Interface for parser text
 */
interface IParser
{
    /**
     * Parser text
     *
     * @param String text for parsing
     * @return array;
     */
    public function parseText(String $text): array;

}
