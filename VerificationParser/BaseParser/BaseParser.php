<?php

namespace VerificationParser\BaseParser;

use VerificationParser\Patterns\IPattern;

/**
 * Base class for parser
 */
abstract class BaseParser implements IParser
{
    /** @var Patterns\IPattern */
    protected $pattern;

    /**
     * Constructor parser
     *
     * @param IPattern instance pattern
     * @return void;
     */
    public function __construct(IPattern $pattern)
    {
        $this->pattern = $pattern;
    }

    /**
     * Parser text
     *
     * @param String text for parsing
     * @return array;
     */
    abstract public function parseText(String $text): array;
}
