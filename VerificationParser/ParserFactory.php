<?php

namespace VerificationParser;

use VerificationParser\BaseParser\IParser;
use VerificationParser\Patterns\IPattern;
use VerificationParser\Exceptions\InterfaceException;

/**
 * Factory instance parser
 */
class ParserFactory
{

    /**
     * Creating instance IParser by instance Pattern
     *
     * @param String name class instance IParser
     * @param String name class instance IPattern
     * @return IParser;
     */
    public static function make(String $parserClass, String $patternClass): IParser
    {
        $pattern = new $patternClass();

        if(!$pattern instanceof IPattern) {
            throw new InterfaceException("Only class instance VerificationParser\BaseParser\IPattern", 1);
        }

        $parser = new $parserClass($pattern);

        if(!$parser instanceof IParser) {
            throw new InterfaceException("Only class instance VerificationParser\BaseParser\IParser", 1);
        }

        return $parser;
    }

}
